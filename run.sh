#!/bin/bash

cd elmer/5mm/ && ElmerSolver && cd ../../
if [ $? -ne 0 ]
then
    exit 1
fi

cd elmer/10mm/ && ElmerSolver && cd ../../
if [ $? -ne 0 ]
then
    exit 1
fi

python/venv/bin/python python/simulate_and_compare.py
if [ $? -ne 0 ]
then
    exit 1
fi

exit 0
