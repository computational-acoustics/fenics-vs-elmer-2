# FEniCS VS Elmer 2

![featured](res/pictures/featured.png)

`Elmer` and `FEniCS` models for the Helmholtz equation applied to the pulsating sphere problem.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the 
[Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Elmer vs FEniCS - Part 2](https://computational-acoustics.gitlab.io/website/posts/36-elmer-vs-fenics-part-2/).

## Study Summary

The pulsating sphere problem is solved with `Elmer` and [`acoupy_helmholtz`](https://gitlab.com/acoupy/acoupy_helmholtz).
The solutions are compared to the exact one for error analysis. Main study parameters are below.

### Domain and Meshes

| Parameter         | Value                       |
|-------------------|-----------------------------|
| Domain Radius     | $0.1$ $\text{m}$            |
| Source Radius     | $5$ $\text{mm}$             |
| Mesh Max Sizes    | $10$, $5$  $\text{mm}$      |
| Mesh Min Sizes    | $1$, $0.1$ $\text{mm}$      |
| Meshing Algorithm | `NETGEN`                    |
| Mesh Order        | $1$                         |
| Element Type      | Second Order $P$-element \* |

\* See [Elmer Solver Manual | Appendix E](https://www.nic.funet.fi/index/elmer/doc/ElmerSolverManual.pdf#appendix.E) 
and the FEniCS's [Periodic Table of the Finite Elements](https://fenicsproject.org/pub/graphics/femtable.pdf) for details.

### Material

| Parameter      | Symbol    | Value                                      |
|----------------|-----------|--------------------------------------------|
| Speed of Sound | $`c`$     | $`343`$ $`\frac{\text{m}}{\text{s}}`$      |
| Density        | $`\rho`$  | $`1.205`$ $`\frac{\text{kg}}{\text{m}^3}`$ |

### Source

| Parameter         | Value                                                                             |
|-------------------|-----------------------------------------------------------------------------------|
| Radius            | $`5`$ $`\text{mm}`$                                                               |
| Normal Velocity   | $`\frac{3}{4} \exp \left( j \frac{\pi}{4} \right)`$ $`\frac{\text{m}}{\text{s}}`$ |

### Simulation

| Parameter         | Value                |
|-------------------|----------------------|
| Frequency         | $`1`$ $`\text{kHz}`$ |
| Termination       | Matched Impedance    |

By matched impedance we mean a Robin boundary condition where the specific impedance $`z`$ of the boundary is matched 
to the outgoing spherical wave impedance.

## Software Overview

The table below reports the software used for this project.

| Software                                                         | Version | Usage                        |
|------------------------------------------------------------------|---------|------------------------------|
| [`Salome Platform`](https://www.salome-platform.org)             | 9.9.0   | Pre-processing               |
| [`Elmer`](http://www.elmerfem.org)                               | 9.0     | Multiphysical solver         |
| [`acoupy_helmholtz`](https://gitlab.com/acoupy/acoupy_helmholtz) | 0.0.3   | Helmholtz Solver             |
| [`Python`](https://www.python.org/)                              | 3.6.9   | Technical Computing Language |
| [`ParaView`](https://www.paraview.org/)                          | 5.10.1  | Post-processing              |

## Repo Structure

* `elmer` contains the `Elmer` study files.
* `error_study` is an output folder for the error analysis script.
* `mesh` contains the `Salome` study and output meshes for `Elmer` (`UNV`) and `acoupy_helmholtz` (`MED`). There is a file for each mesh size.
* `python` contains the error analysis script. This script loads the `Elmer` results, solves for the same problem with `acoupy_helmholtz` and compare both with the exact solution.
* `run.sh` is a bash script to run the study.

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

## How to Run this Study

Follow the steps below to run the study.

Clone the repository and `cd` into the cloned directory:

```bash
git clone https://gitlab.com/computational-acoustics/fenics-vs-elmer-2.git && cd fenics-vs-elmer-2/
```

Follow the installation instructions for [`acoupy_meshutil`](https://gitlab.com/acoupy/acoupy_meshutil) and 
[`acoupy_helmholtz`](https://gitlab.com/acoupy/acoupy_helmholtz). Then, create a local python virtual environment as
follows:

```bash
python3 -m venv --system-site-packages python/venv
```

Now you can run the running script `run.sh`.

Detailed data from `FEniCS`, as well as the exact solution and the `Elmer` solution, are all packaged up in 
`error_study/*.hdf5` files. You can load these files in your technical programming environment of choice. In `Python` use
[`h5py`](https://www.h5py.org/).
